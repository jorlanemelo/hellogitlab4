import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Bem vindo ao Hello Gitlab 4/i);
  expect(linkElement).toBeInTheDocument();
});
